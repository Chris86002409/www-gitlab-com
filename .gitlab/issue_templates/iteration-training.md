
## Iteration Training

[Iteration](https://about.gitlab.com/handbook/values/#iteration) is one of six [GitLab Values](https://about.gitlab.com/handbook/values).  This template is intended to be an onboarding introduction to new team members, and a refresher for those who have been with GitLab.

### Steps

- [ ] Assign this issue to yourself with the title of Iteration Training - First Name Last Name - Q#YYYY

#### Review
  
- [ ] [Minimal Viable Change (MVC)](https://about.gitlab.com/handbook/product/#the-minimally-viable-change-mvc)
- [ ] [Iteration](https://about.gitlab.com/handbook/product/#iteration)
- [ ] [Move Fast](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimum-viable-change)

#### Watch
  
- [ ] [Interview about iteration in engineering with Christopher and Sid](https://www.youtube.com/watch?v=tPTweQlBS54)
- [ ] [Iteration Office Hours with CEO](https://www.youtube.com/watch?v=liI2RKqh-KA)

#### Actions

- [ ] Join the Iteration Office Hours regular meeting on GitLab Team Meeting [Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_6ekbk8ffqnkus3qpj9o26rqejg%40group.calendar.google.com&ctz=America%2FLos_Angeles)

- [ ] If you are a manager, hold an iteration retrospective with your development group
  - [ ] Discuss past examples of successful iteration.  Why was it successful?  How did it meet the definition of an MVC?
  - [ ] Discuss past examples that could use improvement.  How did the example not meet the definition of an MVC? In what ways could you have iterated differently? List specific examples.
  - [ ] Identify areas of improvement for the team and incorporate into your [Product Development Workflow](https://about.gitlab.com/handbook/product-development-flow/)
  - [ ] Update your teams handbook page with ideas and improvements you've incorporated to improve iteration
  - [ ] Update this training template to iterate and improve on our iteration training

#### Questions to ask yourself

- During planning - [is too big to deliver in one month?](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimum-viable-change)  If so, cut the scope.
- At the end of the milestone - did something slip because it was too big?  If so, consider having an iteration retro.
