---
layout: markdown_page
title: "Category Direction - Epics"
---

- TOC
{:toc}

Last Reviewed: 2020-02-18

## Description

Large enterprises are continually working on increasingly more complex and larger
scope initiatives that cut across multiple teams and even departments, spanning months,
quarters, and even years. GitLab's vision is to allow organizing these initiatives
into powerful **multi-level work breakdown** plans and enable **tracking the execution**
of them over time, to be extremely simple and insightful. In addition, GitLab should
highlight which **opportunities have higher ROI**, helping teams make strategic
business planning decisions.

GitLab's **multi-level work breakdown** planning capabilities will include [multiple layers of epics](https://gitlab.com/groups/gitlab-org/-/epics/312)
and [issues](https://gitlab.com/groups/gitlab-org/-/epics/316), allowing enterprises to capture and manage:

- Portfolios, programs, and projects.
- High-level [strategic initiatives and OKRs (objectives and key results)](https://gitlab.com/gitlab-org/gitlab/issues/36775).
- Dependency management both for [epics](https://gitlab.com/groups/gitlab-org/-/epics/2581) and [issues](https://gitlab.com/groups/gitlab-org/-/epics/2032).
- [Project Health/Risk](https://gitlab.com/groups/gitlab-org/-/epics/2204) 

GitLab supports popular [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/),
including Scaled Agile Framework [(SAFe)](https://about.gitlab.com/solutions/agile-delivery/scaled-agile/), [Scrum, and Kanban](https://about.gitlab.com/solutions/project-management/).

## What's next & why

We've written a [mock press release](/direction/plan/portfolio_management/one_year_plan_portfolio_mgmt) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community.

Now that our users can properly build out epics as a collection of associated issues, we are focusing on developing advanced functionality to make Epics more poweful planning artifacts:

- [Track Epic Health Status](https://gitlab.com/gitlab-org/gitlab/issues/199184)
- [Epic Dependency Mapping](https://gitlab.com/groups/gitlab-org/-/epics/2581)
- [Tracking Epic Progress](https://gitlab.com/groups/gitlab-org/-/epics/76)
- [Making it Easier to add Issues and child Epics to Epics](https://gitlab.com/groups/gitlab-org/-/epics/134)

## Competitive landscape

Leveraging Epics as a building block for Portfolio Management is a common use case in the industry, with many are established players such as Jira, Clarity, Planview, VersionOne, AgileCraft, and ServiceNow offering similiar functionality. Many of these tools were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools typically have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. Our strategy is building _toward_ those enterprise use cases and abstractions, starting with the product-development baseline abstractions. And so we are developing multi-level child epics, and adding functionality there incrementally.

## Analyst landscape

We are working with analysts to better understand the space of Portfolio Management as it relates to managing multiple programs and projects. For example, Gartner has an area called [Enteprise Agile Planning Tools](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendors), which this GitLab category is very much a part of.

Analysts in this space are concerned with truly "enterprise" use cases. Agile methodologies have been around for a good number of years, but they have been focused on small teams, or maybe a small number of teams working closely in concert to execute and deliver features. The industry is now focused on how to take these processes that have been proven in small teams, and scale them to large enterprises with business initiatives that span potentially even multiple departments. What is crucial therefore, is how can organizations deliver business value very quickly, but still allow stakeholders (especially executive stakeholders) the visibility to track progress and be assured that the enterprise is working on the right initiatives in the first place. This has even led to the popular [SAFe (Scaled Agile Framework) methodology](https://www.scaledagileframework.com) as a way to help guide enterprise organizations in their Agile transformations.

At GitLab, we embrace this enterprise Agile transformation that companies are going through and want to lead the way by building the tools to do so. In particular, having a single application to view your entire work breakdown structure, from executive level, all the way down to issues and merge requests and even commits, means that our design truly allows for streamlining that visibility. We are focused on building this work breakdown structure out as the initial goal in Agile Portfolio Management.

## Top Customer Success/Sales Issue(s)

To support our Customer Success and Sales departments, we are validating and working towards critical items to enable them to serve additional prospects and customers:

- [Provide Single Tier Epics for Premium](https://gitlab.com/gitlab-org/gitlab/issues/37081)
- [Provide an Epic Swimlane on a Board](https://gitlab.com/gitlab-org/gitlab/issues/7371)
- [Filter Issue list by Epics](https://gitlab.com/groups/gitlab-org/-/epics/2496)
- [Provide Confidential Epics](https://gitlab.com/groups/gitlab-org/-/epics/2435)

## Top User Issue(s)

As the usage of Epics increase and more use cases and needs are being surfaced, we are tracking our most popular issues in the category. In particular, we are focusing on:

- [Epic Swimlanes on Boards](https://gitlab.com/gitlab-org/gitlab/issues/7371)
- [Filter Issue lists by Epic](https://gitlab.com/gitlab-org/gitlab/issues/9029)
- [Project Level Epics](https://gitlab.com/gitlab-org/gitlab/issues/31840)

## Top Vision Item(s)

To drive our vison for Epics and how they supports our overall strategy for Portfolio Management we are continuing to develop the building blocks for deeper more expansive program and portfolio functionality. Currently we are validating and tracking the following big ticket items :

- [Strategic Planning in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/2223)
- [Health/Risk Reporting](https://gitlab.com/gitlab-org/gitlab/issues/202420)
- [Epic Dependency Mapping](https://gitlab.com/groups/gitlab-org/-/epics/2581)
- [Tracking Epic Progress](https://gitlab.com/groups/gitlab-org/-/epics/76)
