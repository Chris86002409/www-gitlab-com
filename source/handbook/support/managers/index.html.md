---
layout: handbook-page-toc
title: Support People
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This section is for information relating to hiring and to the support managers.

## Hiring
See [Support Hiring](hiring.html)

## Metrics

As a Support Management group we should be aware of the tooling we use to generate the metrics that we report on.

1. Sisense
2. Zendesk Explore

## Sisense

Sisense is a general purpose visualization tool that is used by the greater company.
It's extremely powerful with a full SQL interface to our data across functions.
We work with the data team to generate "Stable" Graphs here. Think, KPIs and greater measures that make sense to report to the larger company.
As managers, we will not need to edit these reports often, but we should consuming it regularly.

## Zendesk Explore

Zendesk Explore is a new tool to replace Zendesk Insights. We will use this tool
for quick interactions on new ideas or fact checking Sisense data. Support Managers
should be ready to work with Explore regularly and be comfortable with the tool.

## Why two metrics systems?

Sisense is a company wide tool that is extremely powerful which can make it unwieldy.
Explore gives us an interface that is much easier to navigate and use.
Additionally, Sisense data is a secondary source so it can contain errors. 
By being comfortable using ZD Explore, a primary source, we can make sure that we have accurate data and insights.


## About the Managers

Support Engineering Manager READMEs are found in [Engineering Manager READMEs](/handbook/engineering/readmes/).
