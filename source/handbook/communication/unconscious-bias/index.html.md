---
layout: handbook-page-toc
title: "Unconscious bias"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is unconscious bias?
Unconscious biases are stereotypes about certain groups of people that individuals form outside their own conscious awareness. Nearly all our thoughts and actions are influenced, at least in part, by unconscious impulses. There’s no reason bias should be out of scope.
Categorizing people based on social and other characteristics is a powerful  survival mechanism, as it helps to distinguish friends from foes and make quick "life or death" decisions based on "inner feeling". At the same time this is a fertile ground for growing stereotypes, prejudice, discrimination.
 
Biases help the brain create shortcuts for the decision-making process and detect threats. Our unconscious biases are based on our own experiences and they help us detect patterns and find in-groups, a basic survival mechanism below our conscious radar. If unconscious bias goes unchecked, it can lead to fixed general views of how people should act or behave, and/or negative out-spoken attitude towards a person or group.

Everyone has unconscious biases, the goal is to bring them to our consciousness and navigate them. In order to provide a more inclusive and empathetic work environment.

## How do I recognize it?

* **Gain self-awareness and be present** Pay attention to your body language and tone. Instead of reacting, set up a response. Monitor your self-talk.
* **Exhibit tolerance and patience; avoid assumptions** Choose to listen, pause, and consider first. Offer the benefit of the doubt. Don't jump to conclusions. Ask questions.
* **Make thoughtful impressions and decisions** Challenge your own default views. Take all options into consideration. Use informed decisions, not just your "gut".
* **Seek feedback and choose to use it** Don't avoid the conversation, seek it out. Ask for the feedback you need, not want. Achieve by understanding, not defending.
* **Remain open-minded and continue learning** Hold yourself and others accountable. Revisit, reflect, and review your day. Diversify your circles.

### Seven Unconscious Biases to look out for (in ourselves and in others)

* **[Affinity Bias](https://www.futurelearn.com/courses/diversity-inclusion-awareness/0/steps/39958#targetText=Affinity%20bias%20is%20the%20unconscious,differences%20when%20diversity%20is%20present.)**: Biased towards people "who make me comfortable"; Biased against people "who make me uncomfortable".
* **Affective Heuristic Bias**: Immediate emotional judgement influenced by superficial traits such as race, gender, age, or names
* **[Confirmation Bias](https://www.verywellmind.com/what-is-a-confirmation-bias-2795024#targetText=A%20confirmation%20bias%20is%20a,creative%20than%20right%2Dhanded%20people.)**: People accept ideas or findings which confirm their belief.
* **Conformity Bias**: Tendency to take cues for proper behavior in most context based on the actions of others. __Example:__ studies show that people are more likely to donate to charity if they know/see other doing that.
* **Halo Effect**: Form of bias which favors one aspect that makes a person seem more attractive or desirable. __Example:__  If we think someone is good looking we might also think that s/he is intelligible and charismatic. 
* **Horns Effect**: Opposite to __Halo Effect__. Form of bias that causes one's perception of another to be overly influenced by one or more negative traits. __Example:__  Someone who has failed the project is always like that and incapable if improving.

* **Contrast Effect**: Tendency to promote or demote someone after a single comparison with one or more of their peers.

### Can I test it?
Unconscious bias is far more prevalent than conscious prejudice and often incompatible with one’s conscious values. Therefore it would be good to have an instrument to detect and fight it.
The tool that achieved most popularity both in scientific circles and public is the [implicit-association test (IAT)](https://implicit.harvard.edu/implicit/), which is a collaborative research effort between researchers at Harvard University, the University of Virginia, and University of Washington. It is meant to reveal the strength of one's mental association between certain groups of people and certain traits. It is used to investigate biases in racial groups, gender, sexuality, age, and religion, as well as assessing self-esteem.

Though it has enough [criticism](https://qz.com/1144504/the-world-is-relying-on-a-flawed-psychological-test-to-fight-racism/), IAT can jumpstart our thinking about hidden biases: 
* Where do they come from?
* How do they influence our actions? 
* What can we do about them?

## Practical ways to reduce or avoid the impact of bias
*  As an application reviewer or interviewer, if you find yourself biased positively or negatively, excuse yourself and ask someone else to review or interview that applicant. Being positively biased towards one candidate is unfair to all candidates.


## Resources

#### Inner resources

* [Ongoing discussion](https://gitlab.com/gitlab-com/diversity-and-inclusion/issues/27)
* [This MVC is based on Ashley Sullivan's comment here](https://gitlab.com/gitlab-com/people-ops/General/issues/379#note_208972342)
* [Diversity and Inclusion at GitLab](/company/culture/inclusion/)

#### External resources
* [Teaching Tolerance](https://www.tolerance.org/professional-development/test-yourself-for-hidden-bias)
* [Diversity and Outreach, UCSF](https://diversity.ucsf.edu/resources/unconscious-bias)
* [The Guardian: Unconscious bias: what is it and can it be eliminated?](https://www.theguardian.com/uk-news/2018/dec/02/unconscious-bias-what-is-it-and-can-it-be-eliminated)
* [Wikipedia: IAT](https://en.wikipedia.org/wiki/Implicit-association_test)
* [The world is relying on a flawed psychological test to fight racism](https://qz.com/1144504/the-world-is-relying-on-a-flawed-psychological-test-to-fight-racism/)
* [How to reduce unconscious bias at work](https://lattice.com/library/how-to-reduce-unconscious-bias-at-work)
* [Affinity bias](https://www.futurelearn.com/courses/diversity-inclusion-awareness/0/steps/39958#targetText=Affinity%20bias%20is%20the%20unconscious,differences%20when%20diversity%20is%20present.)
