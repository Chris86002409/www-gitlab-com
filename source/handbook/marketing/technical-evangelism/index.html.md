---
layout: handbook-page-toc
title: "Technical Evangelism"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goal
To build GitLab’s technical brand with deep, meaningful conversations on engineering topics relevant to our industry by leveraging our community of team-members and the wider ecosystem.

### What do we do?

The GitLab technical evangelists are engineers who enjoy learning and teaching the latest technology topics with the wider community. The evangelists are supported by program management within the team and also collaborate closely with the content and PR teams in corporate marketing to amplify their voices. There are two specific goals for the team:

1. **Thought leadership:** Drive GitLab awareness and brand value as full-time technical evangelists participating in thought leadership on au courant topics in the cloud native and cloud computing ecosystem. 

2. **Ecosystem engagement:** Establish GitLab’s technical thought leadership by building GitLab’s influence in the tech community through participation in open source projects, foundations, and other consortiums. A description of consortiums can be found in our [consortiums handbook page](https://about.gitlab.com/handbook/marketing/technical-evangelism/consortiums/)  

3. **Marketing value:** Work with other teams in GitLab (such as content, social, marketing programs) to repurpose, maximize value of, and measure the impact of content created for thought leadership and ecosystem engagement.


## Who we are

### Team members and focus areas
1. [Priyanka Sharma](https://about.gitlab.com/company/team/#pritianka) - Director, Technical Evangelism 
    - Team management
    - Multicloud
    - Cloud native
    - Language skills: English, Hindi 
2. [Abubakar Siddiq Ango](https://about.gitlab.com/company/team/#abuango) - Technical Evangelism Program Manager
    - Program management
        - Running the CFP process for conferences on the [Technical Evagelism events list](https://docs.google.com/spreadsheets/u/1/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?usp=drive_web&ouid=101407496565734994973)
        - Organizing Technical evangelism team's content creation and repurposing efforts
    - Kubernetes
    - Cloud Native
    - Language skills: English, Yoruba, Hausa
3. [Brendan O'Leary](https://about.gitlab.com/company/team/#brendan) -  Senior Developer Evangelist
    - DevOps with a focus on the application developer perspective
        - SCM
        - GitOps
        - CI
        - .NET and Javascript communities
    - Language skills: English
4. [Michael Friedrich](https://about.gitlab.com/company/team/#dnsmichi) - Developer Evangelist
    - DevOps with a focus on the SRE, Ops engineers' perspective
        - CI/CD
        - Serverless
        - Observability
    - Language skills: English, German, Austrian

## How we work

### Request a Technical Evangelist
If you require a Technical Evangelist's help for anything, please use this ["Technical Evangelist Request" issue template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?#technical-evangelist-request) to create an issue and we will get back to as soon as possible. 

### Issue Tracker
We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues) but we own the label `tech-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups. 

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name[]=tech-evangelism).

### Issue Labels
Using the `tech-evangelism` on an issue means we are working on it or participating in the ongoing conversation. `tech-evangelism` label is accompanied by a number of scoped labels we use to organized our work, they are described below:

| `Label` | `Description` |
| ------ | ------ |
| `TE::CFPs` | Technical Evangelism coordinated CFPs |
| `TE::CFP-Draft` | Identifies CFP (Call for Proposal) Drafts that are under Technical Evangelism Team Radar | 
| `TE::CFP-Submitted` | Identifies submitted CFPs (Call for Proposal) and monitored by the Technical Evangelism team |
| `TE::CFP-Review` | Identifies CFP (Call for Proposal) Drafts that requires review by the Technical Evangelism Team | 
| `TE::Rejected` | Identifies Rejected CFPs (Call for Proposal) and monitored by the Technical Evangelism team |
| `TE::CFP-Accepted` | Identifies Accepted CFPs (Call for Proposal) and monitored by the Technical Evangelism team | 
| `TE::DueSoon` | This is used to monitor TE issues are are due soon | 
| `TE::New-Request` | Issues where the services of the Technical Evangelism team has been requested. |
| `TE::Pending-Request` | Requests for the Technical Evangelism team that are currently being looked at or pending for some reason. | 
| `TE::Resolved-Request` | Requests for the Technical Evangelism team that have been resolved or concluded. |
| `TE::Webcast` | Webcasts the Technical Evangelism team is part of or interested in. | 
| `TE::content-draft` | Identifies draft blog posts of the Technical Evangelism team | 
| `TE::content-published` | Published content from the Technical Evangelism team |
| `TE::content-review-request` | Identifies issues where the Technical Evangelism team is needed to review content | 
| `TE::peer-review` | Identifies issues where the Technical Evangelism team is needed to review content |
| `TE::process` | Used for tracking Technical Evangelism administrative issues | 

### Slack
GitLab team members can also reach us at any time on the [#tech-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.


### Our events list
Every year, technical evangelism prioritizes some key events in our ecosystem for which, we run the conference proposal (CFP) process. These events are mainatined in a [living list](https://docs.google.com/spreadsheets/u/1/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit?usp=drive_web&ouid=101407496565734994973) as we add suggestions that fulfill our requirements for focus events. You can also find a calendar view of our events below:

<br>

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_5nsh2o42detjnpana2fheauirg%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>



## Useful links
1. [How to be an evangelist](/handbook/marketing/technical-evangelism/how-to-be-an-evangelist/)
2. [How to submit a successful conference proposal](/handbook/marketing/technical-evangelism/writing-cfps/)
3. [Consortiums we work with](/handbook/marketing/technical-evangelism/consortiums/)
4. [Speaking logistics](/handbook/marketing/technical-evangelism/speaking-logistics/)
