---
layout: handbook-page-toc
title: "Swag Requests from GitLab Team for Outstanding Community Contributions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Let's Award Contributors Together

Each team member is empowered to send GitLab swag to anyone in the wider GitLab community who deserves it.
We'll be using Printfection's giveaway links to make this happen.

## Eligibility
 
Anyone who you feel deserves an extra special something for their contribution, comment, or community involvement is eligible for swag.

This can include, (but is not limited to): 
* blog post authors
* a user providing a tip or trick
* someone who made a video tutorial about GitLab 
* a great answer to on a GitLab StackOverflow question
* detailed contributions or discussions on the GitLab subreddit
* a Power User in our [community forum](https://forum.gitlab.com/)
* users on twitter who open an issue
* people engaging in conversation with the Product team providing insight/feedback on features

For cases not listed above, use your best judgement, and create a merge request to add new cases to the list.


## Giveaway links

The list of the giveaways is found in this [Google Sheets file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) editable only by GitLab team members.

## Sending swag

Once you've decided to award the community contributor, please open the [sheet file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) (only available to GitLab team members) and follow these steps:

* Pick the first available (not used or sent) link in the row
* Fill the 'Sent to Name' field with the contributor's full name
* Fill in the 'Date Sent' field with todays date
* [Optional] Fill the 'Sent to Email' field with the contributor email address
* Fill the 'Requester' field with your name
* Reach out personally to the GitLab contributor with the giveaway URL, and let them know they simply need to pick an item, and input their shipping details 

Note: This data is required for Community Advocates to monitor the placed orders within Printfection.

## Templates for Sending Swag Links

### Email Template

```markdown
Hi [NAME]!

The Community Advocates team at GitLab has noticed the awesome support you provide on [Social Channel] to users asking questions about GitLab.

We'd love to recognize your efforts with some free GitLab swag!
The link below will take you directly to an order form where you can choose your favorite GitLab swag item.

[Giveaway Link]

If you have any questions, don't hesitate to reach out. Thanks for your contributions to the GitLab community!
```

### Twitter Templates

#### Tweets:

```markdown
:clap: Congrats! Send us an email at merch@gitlab.com and we can send some GitLab swag your way! :heart_eyes:
```
#### Direct Messages:
```markdown
We'd love to thank you for your contributions to GitLab StackOverflow questions with some GitLab Swag! You can use this link to place an order, free of charge, and choose your favorite GitLab swag items. Thanks again for supporting StackOverflow users by answering their GitLab questions! 
```


## Finding Community Member Contact Information
This section outlines the strategies that GitLab team member might use to contact community members during this process

* Always default to email communicaiton if possible
* If the user can be found on Twitter, send a direct message via the main GitLab account
* If the user shares social accounts on their GitLab or Stack Overflow profile, send a direct message via the listed platform
* If the user's Stack Overflow handle is the same on GitLab and their email is not public, create an issue and ping them to get their attention



## Include Everyone

If you think that someone deserves a swag prize, feel free to award them with swag via a Printfection link.

## Refresh Giveaway Codes

1. Log into Printfection
2. Navigate to Campaigns -> Giveaways -> Community Contributions
3. Under the section titled 'Giveaway Links' you can see how many links have been created, sent, and redeemed for this campaign
4. Click the 'Get New Link' button to generate a new giveaway link
5. Copy and paste the new link into the [Google Sheets file](https://docs.google.com/spreadsheets/d/1kzK25SE9jPXx50Lw0W8jtm9u77Zr4w13FAg1gyD6Egk/edit?usp=sharing) editable only by GitLab team members
6. When this Google Sheet becomes too long, consider archiving the sheet or creating a new tab with current giveaway links
